<?php
 
namespace app\admin\model;

/**
* 设置模型
*/
class Config extends \think\Model{
    
    protected $type = array(
        'id'  => 'integer',
    );

    protected $auto = array('name', 'update_time', 'status'=>1);
    protected $insert = array('create_time');

    protected function setNameAttr($value){
        return strtolower($value);
    }

    protected function getTypeTextAttr($value, $data){
        $type = config('config_type_list');
        $type_text = explode(',', $type[$data['type']]);
        return $type_text[0];
    }

    public function lists(){
    	$map    = array('status' => 1);
    	$data   = \think\Db::name('Config')->where($map)->field('type,name,value')->select();
    	 
    	$config = array();
    	if($data){ 
    		foreach ($data as $value) {
    			$config[$value['name']] = self::parse($value['type'], $value['value']);
    		}
    	}   
    	return $config; 
    }

    /**
     * 根据配置类型解析配置
     * @param  integer $type  配置类型
     * @param  string  $value 配置值
     * @author 艺品网络  <twothink.cn>
     */
	private static function parse($type, $value){
        switch ($type) {
            case 3: //解析数组
                $array = preg_split('/[,;\r\n]+/', trim($value, ",;\r\n"));
                if(strpos($value,':')){
                    $value  = array();
                    foreach ($array as $val) {
                        list($k, $v) = explode(':', $val);
                        $value[$k]   = $v;
                    }
                }else{
                    $value =    $array;
                }
                break;
        }
        return $value;
    }	
}