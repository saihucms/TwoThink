<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [     
    // +----------------------------------------------------------------------
    // | 用户相关设置 
	// +----------------------------------------------------------------------
    'user_max_cache'     => 1000, //最大缓存用户数
    'user_administrator' => 1, //管理员用户ID 
    // +----------------------------------------------------------------------
    // | 图片上传相关配置
    // +----------------------------------------------------------------------
    'picture_upload' => array(
    		'size'  => 2*1024*1024, //上传的文件大小限制 (0-不做限制)
    		'ext'     => 'jpg,gif,png,jpeg', //允许上传的文件后缀
    		'rootPath' => './Uploads/Picture/', //保存根路径
    		'saveName' => 'date', //上传文件命名规则 date   md5 	 sha1
    		'replace'  => false, //存在同名是否覆盖
    ),
    // +----------------------------------------------------------------------
    // | 文件上传相关配置 
    // +----------------------------------------------------------------------
    'download_upload' => array(
    		'size'  => 2*1024*1024, //上传的文件大小限制 (0-不做限制)
    		'ext'     => 'jpg,gif,png,jpeg,zip,rar,tar,gz,7z,doc,docx,txt,xml', //允许上传的文件后缀
    		'rootPath' => './Uploads/Download/', //保存根路径
    		'saveName' => 'date', //上传文件命名规则 date   md5 	 sha1
    		'replace'  => false, //存在同名是否覆盖
    ),
    // +----------------------------------------------------------------------
    // | 文档模型配置 (文档模型核心配置，请勿更改)
    // +----------------------------------------------------------------------
    'document_model_type' => array(2 => '主题', 1 => '目录', 3 => '段落'),
    // +----------------------------------------------------------------------
    // | 系统数据加密设置
    // +----------------------------------------------------------------------
    'data_auth_key' => 'G!3SkbZxJNj0H}8ptA1`]wu[Ws_dMDoEn^,Um?="', //默认数据加密KEY
    'data_cache_key'=> 'G!3SkbZxJNj0H}8ptA1`]wu[Ws_dMDoEn^,Um?="',
    'app_init'=>array('app\common\addon\InitHookBehavior'),
    // addons配置
//     'addons' => [
//     		// 定义了temphook钩子名，通过temp插件来实现temphook钩子
//     		'temphook' => 'temp'
//     ]
];  